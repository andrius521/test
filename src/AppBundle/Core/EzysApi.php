<?php

namespace AppBundle\Core;

use Exception;

final class EzysApi implements IEzysApi
{
    private $url;

    private $curl = null;

    private $statusCode;

    private $response;

    public function __construct($apiUrl)
    {
        $this->url  = $apiUrl;
        $this->curl = curl_init();

        curl_setopt($this->curl, CURLOPT_HTTPHEADER, [ 'Content-type: application/json' ]);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, true);
    }

    public function __destruct()
    {
        curl_close($this->curl);
    }

    public function get($uri)
    {
        curl_setopt($this->curl, CURLOPT_URL, $this->url.$uri);
        curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, 'GET');

        return $this->execute();
    }
    
    public function post($url)
    {
        //Someday maybe
    }

    private function execute()
    {
        $this->response   = json_decode(curl_exec($this->curl), true);
        $this->statusCode = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);

        if ($this->statusCode !== 200) {
            throw new Exception('Something went wrong');
        }

        return $this;
    }

    public function statusCode()
    {
        return $this->statusCode;
    }

    public function response()
    {
        return $this->response;
    }    
}