<?php

namespace AppBundle\Core;

use Exception;
use Doctrine\DBAL\Connection;

final class Save
{
    private $connection;
    
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }
    
    public function insert($userId, array $cities)
    {        
        $this->connection->insert('saves', [
            'username' => $userId
        ]);
        
        $saveId = $this->connection->lastInsertId();
        
        foreach ($cities as $city) {
            $this->connection->insert('save_cities', [
                'save_id'   => $saveId,
                'city_id'   => $city['id'],
                'city_name' => $city['name']
            ]);  
        }              
    }
    
    public function getAll()
    {
        $saves = $this->connection->createQueryBuilder()
            ->select('*')
            ->from('saves', 's')
            ->execute()
            ->fetchAll();
        
        $savesData = [];
        
        foreach ($saves as $save) {
            $saveCities = $this->connection->createQueryBuilder()
                ->select('s.city_id, s.city_name')
                ->from('save_cities', 's')
                ->where('s.save_id = :save_id')
                ->setParameter('save_id', $save['id'])
                ->execute()
                ->fetchAll();
                
            $savesData[] = [
                'id'        => $save['id'],
                'username'  => $save['username'],
                'cities'    => $saveCities,
                'save_time' => $save['datetime']
            ];
        }
        
        return $savesData;
    }

}