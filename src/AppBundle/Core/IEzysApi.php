<?php

namespace AppBundle\Core;

interface IEzysApi
{
    public function get($uri);
    public function statusCode();
    public function response();
}