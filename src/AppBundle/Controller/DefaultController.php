<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Exception;
use AppBundle\Core\Save;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="mainPage")
     */
    public function indexAction()
    {
        $apiCall = $this->get('app.ezys')->get('content/cities')->response();
        
        if ((int) $apiCall['code'] !== 200) {
            throw new Exception("Service is unavailable right now");
        }   

        $saves = (new Save($this->getDoctrine()->getConnection()))->getAll(); 
        
        return $this->render('default/index.html.twig', [
            'cities' => $apiCall['data'],
            'saves'  => $saves
        ]);
    }
    
    /**
     * @Route("/save-cities", name="saveCities")
     */
    public function saveCitiesAction(Request $request)
    {
        $this->get('session')->start();
        
        $apiCall = $this->get('app.ezys')->get('content/cities')->response();
        
        if ((int) $apiCall['code'] !== 200) {
            throw new Exception("Service is unavailable right now");
        }
        
        foreach($request->get('data') as $saveCity) {
            foreach($apiCall['data'] as $apiCity) {
                if (!array_diff($apiCity, $saveCity)) {
                    continue 2;
                }
            }
            
            throw new Exception("Something went wrong");
        }
        
        (new Save($this->getDoctrine()->getConnection()))
            ->insert($this->get('session')->getId(), $request->get('data'));
        
        return new JsonResponse([
            'status' => 200
        ]);
    }
    
    /**
     * @Route("/saves", name="renderSavedTable")
     */
    public function renderSavedTableAction(Request $request)
    {
        $saves = (new Save($this->getDoctrine()->getConnection()))->getAll(); 
            
        return new JsonResponse([
            'html'   => $this->renderView('default/partials/savesTable.html.twig', ['saves' => $saves]),
            'status' => 200
        ]);               
    }
}
