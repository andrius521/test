function saveSelections() {
    $('.submit-cities').submit(function (e) {
        e.preventDefault();
        
        var cities = $('#cities').val();
        var url    = $('.submit-cities').attr('action');
        var data   = [];
        
        if (!cities)
            return true;
        
        cities.forEach(function(id) {
            var cityName = $('#cities option[value="' + id + '"]').html();
            
            data.push({id : id, name: cityName});
        });
        
        $.post(url, {data}, function(response) {
            if (response.status == '200') {
                $('.alert-success').html('Cities added successfully!');
                $('.alert-success').removeClass('hidden');
                
                renderSaves();
            }
        });
    });
}

function renderSaves() {
    $.get($('base').attr('href') + '/saves', function(response) {
        if (response.status === 200) {
            $('.table').html(response.html);
        }
    });  
}

saveSelections();